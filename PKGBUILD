# Maintainer: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Flamelab <panosfilip@gmail.com

pkgname=gnome-shell-mobile
pkgver=45.0
pkgrel=1
pkgdesc="Next generation desktop shell"
url="https://wiki.gnome.org/Projects/GnomeShell"
arch=(x86_64 aarch64)
license=(GPL)
depends=(
  accountsservice
  gcr-4
  gjs
  gnome-autoar
  gnome-session
  gnome-settings-daemon
  gsettings-desktop-schemas
  gtk4
  libadwaita
  libcanberra-pulse
  libgdm
  libgweather-4
  libibus
  libnma-gtk4
  libsecret
  libsoup3
  mutter-mobile
  unzip
  upower
)
makedepends=(
  asciidoc
  bash-completion
  evolution-data-server
  git
  gnome-control-center
  gobject-introspection
  gtk-doc
  meson
  sassc
)
checkdepends=(
  appstream-glib
  python-dbusmock
  xorg-server-xvfb
)
optdepends=(
  'evolution-data-server: Evolution calendar integration'
  'gnome-bluetooth-3.0: Bluetooth support'
  'gnome-control-center: System settings'
  'gnome-disk-utility: Mount with keyfiles'
  'gst-plugin-pipewire: Screen recording'
  'gst-plugins-good: Screen recording'
  'power-profiles-daemon: Power profile switching'
  'python-gobject: gnome-shell-test-tool performance tester'
  'switcheroo-control: Multi-GPU support'
)
conflicts=(gnome-shell)
provides=(gnome-shell=1:$pkgver)
#groups=(gnome)
#options=(debug)
_commit=259e12cfbe7060cdf63e106d275c0ee0d0158906  # mobile-shell-devel
source=("git+https://gitlab.gnome.org/verdre/mobile-shell.git#commit=$_commit"
        "git+https://gitlab.gnome.org/GNOME/libgnome-volume-control.git")
sha256sums=('SKIP'
            'SKIP')

pkgver() {
  cd gnome-shell
  git describe --tags | sed 's/[^-]*-g/r&/;s/-/+/g'
}

prepare() {
  mv mobile-shell gnome-shell
  cd gnome-shell
  git config --global user.email "build@manjaro.org"
  git config --global user.name "Manjaro Build Server"
  git tag -a 45.0 2127c62b210f605747e019e6e2abee82516e3ccb -m "Bump version to 45.0"

  git submodule init
  git submodule set-url subprojects/gvc "$srcdir/libgnome-volume-control"
  git -c protocol.file.allow=always submodule update
}

build() {
  CFLAGS="${CFLAGS/-O2/-O3} -fno-semantic-interposition"
  LDFLAGS+=" -Wl,-Bsymbolic-functions"

  arch-meson gnome-shell build -D gtk_doc=false
  meson compile -C build
}

_check() (
  mkdir -p -m 700 "${XDG_RUNTIME_DIR:=$PWD/runtime-dir}"
  export XDG_RUNTIME_DIR

  export NO_AT_BRIDGE=1 GTK_A11Y=none

  meson test -C build --print-errorlogs
)

#check() {
#  dbus-run-session xvfb-run -s '-nolisten local +iglx -noreset' \
#    bash -c "$(declare -f _check); _check"
#}

package() {
  depends+=(libmutter-13.so)
  meson install -C build --destdir "$pkgdir"
}

# vim:set sw=2 et:
